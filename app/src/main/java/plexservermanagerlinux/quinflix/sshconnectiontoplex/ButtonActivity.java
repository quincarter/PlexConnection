package plexservermanagerlinux.quinflix.sshconnectiontoplex;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by Quin on 2/27/2016.
 */
public class ButtonActivity extends Activity {//begin class
    public final String plexUpdate = "plexmediaserver_0.9.15.6.1714-7be11e1_amd64.deb";

    public void buttonsMethod(final String buttonAction) {//begin buttons method
        new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {//begin do in background
                String command = "";
                try {
                    command = getCommandForButton(buttonAction);

                    String username = "root", password = "rocket", server = "lqcnetwork.ddns.net";
                    JSch jsch = new JSch();
                    Session session = jsch.getSession(username, server, 22255);
                    session.setPassword(password);

                    // Avoid asking for key confirmation
                    Properties prop = new Properties();
                    prop.put("StrictHostKeyChecking", "no");
                    session.setConfig(prop);

                    session.connect(10000);


                    ChannelExec channel = (ChannelExec) session.openChannel("exec");
                    BufferedReader in = new BufferedReader(new InputStreamReader(channel.getInputStream()));

                    channel.setCommand(command); //could be "pwd;" or "sudo shutdown -h now"
                    channel.connect();

                    String msg = null;
                    String wholemessage = "";

                    while ((msg = in.readLine()) != null) {
                        wholemessage += msg + "\n";

                    }

                    Thread.sleep(8000);
                    channel.getErrStream();
                    channel.disconnect();
                    //session.disconnect();


                    final String thereturnresult = wholemessage;
                    final String tempcommand = command;

                    /*
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), thereturnresult, Toast.LENGTH_SHORT).show();
                        }
                    });*/
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSchException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }//end do in background
        }.execute(1);
    }//end buttonsMethod

    public String getCommandForButton(final String buttonAction)
    {//begin method to get the command for the button
        String command = "";
        switch (buttonAction)
        {//begin switch statement
            case "updateos":
            {//begin updateos case
                command = "apt-get update && apt-get upgrade -y --force-yes -qq && apt-get dist-upgrade -y --force-yes -qq && exit";
                break;
            }//end updateos case

            case "updateplex":
            {//begin updateos case

                command = "./plexupdate.sh -a && exit";
                break;
            }//end updateos case

            case "rebootserver":
            {//begin updateos case
                command = "reboot";
                break;
            }//end updateos case

            case "refreshupdates":
            {//begin updateos case
                command = "apt-get update && exit";
                break;
            }//end updateos case

        }//end switch statement

        return command;//returns the specified command to the command string
    }//end method to get the command for the button

    }//end class
