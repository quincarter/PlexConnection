package plexservermanagerlinux.quinflix.sshconnectiontoplex;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class MainActivity extends AppCompatActivity
{//begin main activity
    static Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {//begin on create
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButtonActivity ba = new ButtonActivity();
    }//end on create

    public void OSUpdatesButtonCommand(View view)
    {
        String buttonAction = "updateos";
        ButtonActivity ba = new ButtonActivity();
        ba.buttonsMethod(buttonAction);
        Toast.makeText(MainActivity.this, "Running OS Updates", Toast.LENGTH_LONG).show();
        ProgressBar.generateViewId();
    }

    public void plexServerUpdatesButtonCommand(View view)
    {
        String buttonAction = "updateplex";
        ButtonActivity ba = new ButtonActivity();
        ba.buttonsMethod(buttonAction);
        Toast.makeText(MainActivity.this, "Checking for Updates and Updating Plex", Toast.LENGTH_LONG).show();
    }

    public void rebootServerButtonCommand(View view)
    {
        String buttonAction = "rebootserver";
        ButtonActivity ba = new ButtonActivity();
        ba.buttonsMethod(buttonAction);
        Toast.makeText(MainActivity.this, "Rebooting Plex Server Now", Toast.LENGTH_SHORT).show();

    }

    public void aptGetUpdateButtonCommand(View view)
    {
        String buttonAction = "refreshupdates";
        ButtonActivity ba = new ButtonActivity();
        ba.buttonsMethod(buttonAction);
        Toast.makeText(MainActivity.this, "Refreshing updates", Toast.LENGTH_SHORT).show();
    }

    public void scanMovieDB(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lqcnetwork.ddns.net:32400/library/sections/1/refresh?X-Plex-Token=GsQ6RKAesymnAtGudpjb"));
        startActivity(browserIntent);
    }

    public void scanTVDB(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lqcnetwork.ddns.net:32400/library/sections/2/refresh?X-Plex-Token=GsQ6RKAesymnAtGudpjb"));
        startActivity(browserIntent);
    }

    public void scanMusicDB(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lqcnetwork.ddns.net:32400/library/sections/7/refresh?X-Plex-Token=GsQ6RKAesymnAtGudpjb"));
        startActivity(browserIntent);
    }

    public void scanAllSections(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lqcnetwork.ddns.net:32400/library/sections/all/refresh?X-Plex-Token=GsQ6RKAesymnAtGudpjb"));
        startActivity(browserIntent);
    }

}//end main activity